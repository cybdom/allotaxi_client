package com.cybdom.allotaxi.classes;

public class userProfileClass {
    String user_nom, user_prenom, user_gender, user_email, user_date_naissance;
    Double user_note;

    public userProfileClass(){}

    public userProfileClass(String user_nom, String user_prenom, String user_gender, String user_email, String user_date_naissance, Double user_note) {
        this.user_nom = user_nom;
        this.user_prenom = user_prenom;
        this.user_gender = user_gender;
        this.user_email = user_email;
        this.user_date_naissance = user_date_naissance;
        this.user_note = user_note;
    }

    public String getUser_nom() {
        return user_nom;
    }

    public void setUser_nom(String user_nom) {
        this.user_nom = user_nom;
    }

    public String getUser_prenom() {
        return user_prenom;
    }

    public void setUser_prenom(String user_prenom) {
        this.user_prenom = user_prenom;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_date_naissance() {
        return user_date_naissance;
    }

    public void setUser_date_naissance(String user_date_naissance) {
        this.user_date_naissance = user_date_naissance;
    }

    public Double getUser_note() {
        return user_note;
    }

    public void setUser_note(Double user_note) {
        this.user_note = user_note;
    }
}
