package com.cybdom.allotaxi.classes;

public class couponClass {
    double couponValue;
    String couponText,couponTimespaneValidity;
    public couponClass(){}

    public couponClass(double couponValue, String couponText, String couponTimespaneValidity) {
        this.couponValue = couponValue;
        this.couponText = couponText;
        this.couponTimespaneValidity = couponTimespaneValidity;
    }

    public double getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(double couponValue) {
        this.couponValue = couponValue;
    }

    public String getCouponText() {
        return couponText;
    }

    public void setCouponText(String couponText) {
        this.couponText = couponText;
    }

    public String getCouponTimespaneValidity() {
        return couponTimespaneValidity;
    }

    public void setCouponTimespaneValidity(String couponTimespaneValidity) {
        this.couponTimespaneValidity = couponTimespaneValidity;
    }
}
