package com.cybdom.allotaxi.classes;

public class ChauffeurClass {
    String driver_nom,
        driver_prenom,
        driver_phone_number,
        driver_note,
        driver_voiture,
        driver_immatriculation;
    public ChauffeurClass(){}
    public ChauffeurClass(String driver_nom, String driver_prenom, String driver_phone_number, String driver_note, String driver_voiture, String driver_immatriculation) {
        this.driver_nom = driver_nom;
        this.driver_prenom = driver_prenom;
        this.driver_phone_number = driver_phone_number;
        this.driver_note = driver_note;
        this.driver_voiture = driver_voiture;
        this.driver_immatriculation = driver_immatriculation;
    }

    public String getDriver_nom() {
        return driver_nom;
    }

    public void setDriver_nom(String driver_nom) {
        this.driver_nom = driver_nom;
    }

    public String getDriver_prenom() {
        return driver_prenom;
    }

    public void setDriver_prenom(String driver_prenom) {
        this.driver_prenom = driver_prenom;
    }

    public String getDriver_phone_number() {
        return driver_phone_number;
    }

    public void setDriver_phone_number(String driver_phone_number) {
        this.driver_phone_number = driver_phone_number;
    }

    public String getDriver_note() {
        return driver_note;
    }

    public void setDriver_note(String driver_note) {
        this.driver_note = driver_note;
    }

    public String getDriver_voiture() {
        return driver_voiture;
    }

    public void setDriver_voiture(String driver_voiture) {
        this.driver_voiture = driver_voiture;
    }

    public String getDriver_immatriculation() {
        return driver_immatriculation;
    }

    public void setDriver_immatriculation(String driver_immatriculation) {
        this.driver_immatriculation = driver_immatriculation;
    }
}
