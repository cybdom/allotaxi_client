package com.cybdom.allotaxi.classes;

public class CourseClass {
    String client_Id, destination_lat, destination_lng, origin_lat, origin_lng, course_Price, course_Duration, chauffeur_id, course_date
            ,accepted_course_id;
    Boolean is_accepted;
    public CourseClass(){}

    public CourseClass(String client_Id, String destination_lat, String destination_lng, String origin_lat, String origin_lng, String course_Price, String course_Duration, String chauffeur_id, String course_date, String accepted_course_id, Boolean is_accepted) {
        this.client_Id = client_Id;
        this.destination_lat = destination_lat;
        this.destination_lng = destination_lng;
        this.origin_lat = origin_lat;
        this.origin_lng = origin_lng;
        this.course_Price = course_Price;
        this.course_Duration = course_Duration;
        this.chauffeur_id = chauffeur_id;
        this.course_date = course_date;
        this.accepted_course_id = accepted_course_id;
        this.is_accepted = is_accepted;
    }

    public String getClient_Id() {
        return client_Id;
    }

    public void setClient_Id(String client_Id) {
        this.client_Id = client_Id;
    }

    public String getDestination_lat() {
        return destination_lat;
    }

    public void setDestination_lat(String destination_lat) {
        this.destination_lat = destination_lat;
    }

    public String getDestination_lng() {
        return destination_lng;
    }

    public void setDestination_lng(String destination_lng) {
        this.destination_lng = destination_lng;
    }

    public String getOrigin_lat() {
        return origin_lat;
    }

    public void setOrigin_lat(String origin_lat) {
        this.origin_lat = origin_lat;
    }

    public String getOrigin_lng() {
        return origin_lng;
    }

    public void setOrigin_lng(String origin_lng) {
        this.origin_lng = origin_lng;
    }

    public String getCourse_Price() {
        return course_Price;
    }

    public void setCourse_Price(String course_Price) {
        this.course_Price = course_Price;
    }

    public String getCourse_Duration() {
        return course_Duration;
    }

    public void setCourse_Duration(String course_Duration) {
        this.course_Duration = course_Duration;
    }

    public String getChauffeur_id() {
        return chauffeur_id;
    }

    public void setChauffeur_id(String chauffeur_id) {
        this.chauffeur_id = chauffeur_id;
    }

    public String getCourse_date() {
        return course_date;
    }

    public void setCourse_date(String course_date) {
        this.course_date = course_date;
    }

    public String getAccepted_course_id() {
        return accepted_course_id;
    }

    public void setAccepted_course_id(String accepted_course_id) {
        this.accepted_course_id = accepted_course_id;
    }

    public Boolean getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(Boolean is_accepted) {
        this.is_accepted = is_accepted;
    }
}
