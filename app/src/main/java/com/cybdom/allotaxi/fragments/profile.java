package com.cybdom.allotaxi.fragments;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cybdom.allotaxi.R;
import com.cybdom.allotaxi.classes.userProfileClass;
import com.cybdom.allotaxi.login;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class profile extends Fragment {

    View parentView;
    FirebaseUser user;
    DatabaseReference user_database_ref = null;
    userProfileClass userProfile = new userProfileClass();

    private int year, month, day;

    EditText user_nom, user_prenom, user_date_naissance;
    Button btn_save_profile, btn_logout;
    TextView user_phone, user_note;

    public profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if ( user != null ) {
            user_database_ref =  FirebaseDatabase.getInstance().getReference().child("client").child(user.getUid()).child("profile");
        }
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_profile, container, false);

        user_nom = parentView.findViewById(R.id.user_nom);
        user_prenom = parentView.findViewById(R.id.user_prenom);
        user_date_naissance = parentView.findViewById(R.id.user_date_naissance);
        btn_save_profile = parentView.findViewById(R.id.btn_save_profile);
        btn_logout = parentView.findViewById(R.id.btn_logout);
        user_phone = parentView.findViewById(R.id.user_phone);
        user_note = parentView.findViewById(R.id.user_note);

        return parentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btn_save_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userProfile.setUser_nom(user_nom.getText().toString());
                userProfile.setUser_prenom(user_prenom.getText().toString());
                userProfile.setUser_date_naissance(user_date_naissance.getText().toString());

                if ( validateUserProfile() ){
                    Map<String, Object> userUpdates = new HashMap<>();

                    userUpdates.put("user_nom", userProfile.getUser_nom());
                    userUpdates.put("user_prenom", userProfile.getUser_nom());
                    userUpdates.put("user_date_naissance", userProfile.getUser_nom());

                    user_database_ref.updateChildren(userUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(parentView.getContext(), "Votre profile a etais mis a jour", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(getActivity(), login.class);
                startActivity(i);
                getActivity().finish();
            }
        });
        user_date_naissance.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if ( hasFocus ) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    showDate(dayOfMonth, monthOfYear, year);

                                }
                            }, year, month, day);
                    datePickerDialog.show();
                }
            }
        });

        if ( user != null ){
            user_database_ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    userProfile = dataSnapshot.getValue(userProfileClass.class);
                    setValues();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void setValues(){
        user_nom.setText(userProfile.getUser_nom());
        user_prenom.setText(userProfile.getUser_prenom());
        user_date_naissance.setText(userProfile.getUser_date_naissance());
        user_phone.setText(user.getPhoneNumber());
        user_note.setText(String.valueOf(userProfile.getUser_note()));
    }

    private boolean validateUserProfile(){

        if ( userProfile.getUser_nom().isEmpty() || userProfile.getUser_prenom().isEmpty() || userProfile.getUser_date_naissance().isEmpty() ){
            return false;
        }

        return true;
    }

    private void showDate(int year, int month, int day) {
        EditText user_date_naissance = parentView.findViewById(R.id.user_date_naissance);
        month += 1;
        user_date_naissance.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }
}
