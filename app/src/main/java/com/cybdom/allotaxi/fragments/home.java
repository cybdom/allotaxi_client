package com.cybdom.allotaxi.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cybdom.allotaxi.R;
import com.cybdom.allotaxi.classes.ChauffeurClass;
import com.cybdom.allotaxi.classes.CourseClass;
import com.cybdom.allotaxi.classes.couponClass;
import com.cybdom.allotaxi.functions.GetDate;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.android.telemetry.MapboxTelemetry;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

/**
 * A simple {@link Fragment} subclass.
 */
public class home extends Fragment  //implements OnMapReadyCallback
{
    private MapboxTelemetry mapboxTelemetry;
    private PermissionsManager permissionsManager;

    private static final int REQUEST_CODE_LOCAL_LOCATION = 5678;
    private static final int REQUEST_CODE_DESTINATION_LOCATION = 5677;

    CardView localAddressCard, DestinationAddressCard, couponCard;
    Button btn_submit, btn_cancel;
    ImageButton plan_course;
    LinearLayout first_process,
            voyage_info,
            second_voyage_info,
            second_process,
            layout_set_coupon,
            layout_coupon_result,
            layout_recherche_chauffeur;

    private static final String ROUTE_LAYER_ID = "route-layer-id";
    private static final String ROUTE_SOURCE_ID = "route-source-id";
    private static final String ICON_LAYER_ID = "icon-layer-id";
    private static final String ICON_SOURCE_ID = "icon-source-id";
    private static final String RED_PIN_ICON_ID = "red-pin-icon-id";

    private String text_distance = "0";
    private String text_duration = "0";
    private String text_price = "0";

    private MapView mapView;
    public static MapboxMap mapboxMap;
    private DirectionsRoute currentRoute;
    private MapboxDirections client;
    private static Point origin = null;
    private Point destination = null;
    View parentView = null;
    static TextView address_local, address_destination, coupon_value;
    CourseClass mCourseClass;
    couponClass coupon;
    Boolean is_coupon_valid = false;

    DatabaseReference courseDatabaseRef = null;
    DatabaseReference acceptedCourseDatabaserRef = null;

    int progress_step = 0;

    public home() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), "pk.eyJ1IjoiY3liZG9tIiwiYSI6ImNqc2lvbHJuNTBtdDc0NGxwZWR4bWlpZWcifQ.mUOeq_PPh9eq4I2ezNaDPw");
        Log.i("TRACE", "OnCreate Home");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_home, container, false);

        first_process = parentView.findViewById(R.id.first_process);
        voyage_info = parentView.findViewById(R.id.voyage_info);
        second_voyage_info = parentView.findViewById(R.id.second_voyage_info);
        second_process = parentView.findViewById(R.id.second_process);
        couponCard = parentView.findViewById(R.id.couponCard);
        layout_set_coupon = parentView.findViewById(R.id.layout_set_coupon);
        layout_coupon_result = parentView.findViewById(R.id.layout_coupon_result);

        layout_recherche_chauffeur = parentView.findViewById(R.id.layout_recherche_chauffeur);

        coupon_value = parentView.findViewById(R.id.coupon_value);

        localAddressCard = parentView.findViewById(R.id.setLocalAddress);
        DestinationAddressCard = parentView.findViewById(R.id.setDestination);

        btn_submit = parentView.findViewById(R.id.submit);
        btn_cancel = parentView.findViewById(R.id.cancel);

        plan_course = parentView.findViewById(R.id.plan_course);
        address_local = parentView.findViewById(R.id.address_local);
        address_destination = parentView.findViewById(R.id.address_destination);

        localAddressCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocalLocation(v);
            }
        });
        DestinationAddressCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                setDestinationAddressCard(v);            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                proceed(v);            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_course(v);
            }
        });

        Button validate_coupon = parentView.findViewById(R.id.validate_coupon);

        validate_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateCoupon();
            }
        });
        Log.i("TRACE", "OnCreateView Home");
        return parentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String accessTokenTelemetry = obtainAccessToken();
        String userAgentTelemetry = "MapboxEventsAndroid/4.1.0";
        mapboxTelemetry = new MapboxTelemetry(parentView.getContext(), accessTokenTelemetry, userAgentTelemetry);

        mapView = parentView.findViewById(R.id.mapView);
        mapView.onCreate(null);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap maPboxMap) {
                mapboxMap = maPboxMap;
                if ( progress_step == 0 ) {
                    mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {
                            enableLocationComponent(style, getContext());
                        }
                    });
                }
            }
        });
        Log.i("TRACE", "onActivityCreated Home");
    }

    private void initSource(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource(ROUTE_SOURCE_ID,
                FeatureCollection.fromFeatures(new Feature[] {})));

        GeoJsonSource iconGeoJsonSource = new GeoJsonSource(ICON_SOURCE_ID, FeatureCollection.fromFeatures(new Feature[] {
                Feature.fromGeometry(Point.fromLngLat(origin.longitude(), origin.latitude())),
                Feature.fromGeometry(Point.fromLngLat(destination.longitude(), destination.latitude()))}));
        loadedMapStyle.addSource(iconGeoJsonSource);
    }

    /**
     * Add the route and maker icon layers to the map
     */
    private void initLayers(@NonNull Style loadedMapStyle) {
        LineLayer routeLayer = new LineLayer(ROUTE_LAYER_ID, ROUTE_SOURCE_ID);

// Add the LineLayer to the map. This layer will display the directions route.
        routeLayer.setProperties(
                lineCap(Property.LINE_CAP_ROUND),
                lineJoin(Property.LINE_JOIN_ROUND),
                lineWidth(5f),
                lineColor(Color.parseColor("#009688"))
        );
        loadedMapStyle.addLayer(routeLayer);

// Add the red marker icon image to the map
        loadedMapStyle.addImage(RED_PIN_ICON_ID, BitmapUtils.getBitmapFromDrawable(
                getResources().getDrawable(R.drawable.red_marker)));

// Add the red marker icon SymbolLayer to the map
        loadedMapStyle.addLayer(new SymbolLayer(ICON_LAYER_ID, ICON_SOURCE_ID).withProperties(
                iconImage(RED_PIN_ICON_ID),
                iconIgnorePlacement(true),
                iconIgnorePlacement(true),
                iconOffset(new Float[] {0f, -4f})));
    }

    private void getRoute(@NonNull final Style style, final Point origin, final Point destination) {

        client = MapboxDirections.builder()
                .language(Locale.FRANCE)
                .origin(origin)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                System.out.println(call.request().url().toString());
// You can get the generic HTTP info about the response
                Timber.d("Response code: " + response.code());
                if (response.body() == null) {
                    Timber.e("No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Timber.e("No routes found");
                    return;
                }

// Get the directions route
                currentRoute = response.body().routes().get(0);
                double distance, duration, price;
                int hours = currentRoute.duration().intValue() / 3600;
                int minutes = (currentRoute.duration().intValue() % 3600) / 60;
                int seconds = currentRoute.duration().intValue() % 60;
                distance = currentRoute.distance() / 1000;
                price = Math.round(distance * 3.4);
                Log.i("Original price", String.valueOf(price));
                if ( is_coupon_valid ){
                    Log.i("Coupon Value", String.valueOf(coupon.getCouponValue()));
                    price = price - ( price * coupon.getCouponValue() / 100 );
                    Log.i("Reduced price", String.valueOf(price));
                }

                text_distance = String.valueOf(currentRoute.distance());
                text_duration =  String.format("%02d:%02d:%02d", hours, minutes, seconds);
                text_price = String.valueOf(price);

                showVoyageInfo(text_distance, text_duration, text_price);

                if (style.isFullyLoaded()) {
// Retrieve and update the source designated for showing the directions route
                    GeoJsonSource source = style.getSourceAs(ROUTE_SOURCE_ID);
// Create a LineString with the directions route's geometry and
// reset the GeoJSON source for the route LineLayer source
                    if (source != null) {
                        Timber.d("onResponse: source != null");
                        source.setGeoJson(FeatureCollection.fromFeature(
                                Feature.fromGeometry(LineString.fromPolyline(currentRoute.geometry(), PRECISION_6))));
                    }
                }
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                Timber.e("Error: " + throwable.getMessage());
                Toast.makeText(getActivity(), "Error: " + throwable.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setDestinationAddressCard(View v) {
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(Mapbox.getAccessToken())
                .placeOptions(PlaceOptions.builder()
                        .country("DZ")
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS))
                .build(getActivity());
        startActivityForResult(intent, REQUEST_CODE_DESTINATION_LOCATION);
    }

    public void setLocalLocation(View v){
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(Mapbox.getAccessToken())
                .placeOptions(PlaceOptions.builder()
                        .country("DZ")
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS))
                .build(getActivity());
        startActivityForResult(intent, REQUEST_CODE_LOCAL_LOCATION);
    }

    public void proceed(View v){
        if ( progress_step == 0 && destination != null && origin != null ) {

            mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    initSource(style);

                    initLayers(style);

                    Log.i("TRACE", "Get Routes");

                    getRoute(style, origin, destination);

                    localAddressCard.setOnClickListener(null);
                    DestinationAddressCard.setOnClickListener(null);

                    progress_step = 1;

                }
            });
        } else if ( progress_step == 1 ){
            sendCourseToDatabase();
        }
    }

    public void cancel_course(View v){
        Toast.makeText(v.getContext(), "Clicked CAncel", Toast.LENGTH_SHORT).show();
        if ( courseDatabaseRef != null ){
            Log.i("CANCEL", "CourseDatabaseRef not null so will be removed");
            courseDatabaseRef.removeValue();
        }
        if ( acceptedCourseDatabaserRef != null ){
            Log.i("CANCEL", "acceptedCourseDatabaserRef not null so will be removed");
            acceptedCourseDatabaserRef.removeValue();
        }
        first_process.setVisibility(View.VISIBLE);
        couponCard.setVisibility(View.VISIBLE);
        layout_set_coupon.setVisibility(View.VISIBLE);
        layout_coupon_result.setVisibility(View.GONE);
        voyage_info.setVisibility(View.GONE);
        second_process.setVisibility(View.GONE);
        localAddressCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocalLocation(v);
            }
        });
        DestinationAddressCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                setDestinationAddressCard(v);            }
        });
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onStart() {
        super.onStart();
        mapView.onStart();
        Log.i("TRACE", "onStart Home");
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        Log.i("TRACE", "onResume Home");
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        Log.i("TRACE", "onPause Home");
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        Log.i("TRACE", "onStop Home");
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private String obtainAccessToken() {
        return getString(R.string.mapbox_access_token);
    }

//    @Override
//    public void onMapReady(@NonNull MapboxMap mapboxMap) {
//        home.mapboxMap = mapboxMap;
//        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
//            @Override
//            public void onStyleLoaded(@NonNull Style style) {
//                enableLocationComponent(style, parentView.getContext());
//            }
//        });
//        Log.i("Map", "onMapReady");
//    }

    @SuppressWarnings( {"MissingPermission"})
    public static void enableLocationComponent(@NonNull Style loadedMapStyle, Context context) {

        Log.i("TRACE", "enableLocationComponent");
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(context)) {
            Log.i("TRACE", "areLocationPermissionsGranted");

// Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

// Activate with options
            locationComponent.activateLocationComponent(context, loadedMapStyle);

// Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

// Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);
            if (locationComponent.getLastKnownLocation() != null) {
                origin = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                        locationComponent.getLastKnownLocation().getLatitude());
                makeGeocodeSearch(new LatLng(origin.latitude(), origin.longitude()), context);
            }

        }
    }

    @Override

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TRACE", "onActivityResult");

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_LOCAL_LOCATION) {

            CarmenFeature feature = PlaceAutocomplete.getPlace(data);
            setAddressOnCard(REQUEST_CODE_LOCAL_LOCATION, feature.placeName());
            origin = Point.fromJson(feature.geometry().toJson());
            mapboxMap.setCameraPosition(new CameraPosition.Builder()
                    .target(new LatLng(origin.latitude(), origin.longitude()))
                    .zoom(10)
                    .build());

        } else if ( resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_DESTINATION_LOCATION){

            CarmenFeature feature = PlaceAutocomplete.getPlace(data);
            setAddressOnCard(REQUEST_CODE_DESTINATION_LOCATION, feature.placeName());
            destination = Point.fromJson(feature.geometry().toJson());
            Log.i("destination", destination.toJson());

        }
    }

    public void setAddressOnCard(int requestCode, String address){
        if ( requestCode == REQUEST_CODE_LOCAL_LOCATION ) {
            address_local.setText(address);
        } else if (requestCode == REQUEST_CODE_DESTINATION_LOCATION){
            address_destination.setText(address);
        }
    }

    public static void makeGeocodeSearch(LatLng location, Context context){
        try {
// Build a Mapbox geocoding request
            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(context.getString(R.string.mapbox_access_token))
                    .query(Point.fromLngLat(location.getLongitude(), location.getLatitude()))
                    .geocodingTypes(GeocodingCriteria.TYPE_PLACE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    List<CarmenFeature> results = response.body().features();
                    if (results.size() > 0) {

// Get the first Feature from the successful geocoding response
                        CarmenFeature feature = results.get(0);
                        address_local.setText(feature.placeName());
                    } else {
                        address_local.setText("ADDRESS INCONNU");
                        Log.i("geocoding result", "INCONNU");
                    }
                }
                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }

    public void showVoyageInfo(String text_distance,String  text_duration,String  text_price){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        voyage_info.setVisibility(View.VISIBLE);
        layout_set_coupon.setVisibility(View.GONE);

        if ( is_coupon_valid ) {
            layout_coupon_result.setVisibility(View.VISIBLE);
            coupon_value.setText(String.valueOf(coupon.getCouponValue()) + "%");
        } else {
            couponCard.setVisibility(View.GONE);
        }

        btn_submit.setText("Demander un chauffeur");

        mCourseClass = new CourseClass(user.getUid(),
                String.valueOf(destination.latitude()),
                String.valueOf(destination.longitude()),
                String.valueOf(origin.latitude()),
                String.valueOf(origin.longitude()),
                text_price,
                text_duration,
                "",
                GetDate.getDate(),
                "",
                false);

        TextView textViewPrice, textViewDuration;
        textViewPrice = parentView.findViewById(R.id.text_price);
        textViewDuration = parentView.findViewById(R.id.text_duration);

        textViewPrice.setText(text_price);
        textViewDuration.setText(text_duration);
    }

    public void sendCourseToDatabase(){
        layout_recherche_chauffeur.setVisibility(View.VISIBLE);

        final String courseKey = FirebaseDatabase.getInstance().getReference()
                .child("COURSES")
                .child("REQUESTED")
                .push().getKey();
        courseDatabaseRef = FirebaseDatabase.getInstance().getReference()
                .child("COURSES")
                .child("REQUESTED")
                .child(courseKey);

        courseDatabaseRef.setValue(mCourseClass).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                FirebaseDatabase.getInstance().getReference()
                        .child("COURSES")
                        .child("REQUESTED")
                        .child(courseKey)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                CourseClass courseClass = dataSnapshot.getValue(CourseClass.class);
                                if ( courseClass.getIs_accepted() ) {
                                    getAcceptedCourse(courseClass.getChauffeur_id(), courseClass.getAccepted_course_id());
                                    courseDatabaseRef.removeValue();
                                    courseDatabaseRef.removeEventListener(this);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }
        });


    }

    public void getAcceptedCourse(String chauffeurId, String courseKey){
        Log.i("ACCEPTED COURSE", courseKey);

        final TextView accepted_text_price = parentView.findViewById(R.id.accepted_text_price),
                accepted_text_duration = parentView.findViewById(R.id.accepted_text_duration);

        if ( !chauffeurId.isEmpty() && !courseKey.isEmpty()){
            layout_recherche_chauffeur.setVisibility(View.GONE);
            acceptedCourseDatabaserRef = FirebaseDatabase.getInstance().getReference()
                    .child("COURSES")
                    .child("ACCEPTED")
                    .child(courseKey);
            FirebaseDatabase.getInstance().getReference().child("DRIVERS").child(chauffeurId).child("public").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    ChauffeurClass chauffeurClass = dataSnapshot.getValue(ChauffeurClass.class);
                    first_process.setVisibility(View.GONE);
                    second_process.setVisibility(View.VISIBLE);

                    TextView driver_immatriculation, driver_name, driver_car;
                    driver_immatriculation = parentView.findViewById(R.id.driver_immatriculation);
                    driver_name = parentView.findViewById(R.id.driver_name);
                    driver_car = parentView.findViewById(R.id.driver_car);

                    driver_immatriculation.setText(chauffeurClass.getDriver_immatriculation());
                    driver_name.setText(chauffeurClass.getDriver_nom() + " " + chauffeurClass.getDriver_prenom());
                    driver_car.setText(chauffeurClass.getDriver_voiture());


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            acceptedCourseDatabaserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    CourseClass acceptedCourseContent = dataSnapshot.getValue(CourseClass.class);
                    accepted_text_price.setText(acceptedCourseContent.getCourse_Price());
                    accepted_text_duration.setText(acceptedCourseContent.getCourse_Duration());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void validateCoupon(){
        final EditText coupon_text = parentView.findViewById(R.id.coupon_text);
        if ( !coupon_text.getText().toString().isEmpty() ){
            FirebaseDatabase.getInstance().getReference().child("COUPON").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                        coupon = postSnapshot.getValue(couponClass.class);
                        if ( coupon.getCouponText().equals(coupon_text.getText().toString()) ){
                            is_coupon_valid = true;
                            Toast.makeText(parentView.getContext(), "Votre coupon a etais valider!", Toast.LENGTH_SHORT).show();
                            break;
                        }
                        if ( !is_coupon_valid ){
                            Toast.makeText(parentView.getContext(), "Coupon Invalid.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(parentView.getContext(), "La demande a etais annuler.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}