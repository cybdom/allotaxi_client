package com.cybdom.allotaxi.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.allotaxi.R;
import com.cybdom.allotaxi.adapters.courseHistoryAdapter;
import com.cybdom.allotaxi.classes.CourseClass;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class course_history extends Fragment {
    View parentView;
    private RecyclerView rv;
    private List<CourseClass> mCourseClass;

    public course_history() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_course_history, container, false);

        rv= parentView.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mCourseClass = new ArrayList<>();
        mCourseClass.add( new CourseClass("",
                "",
                "",
                "",
                "",
                "2000DZD",
                "02:01:00",
                "",
                "27/02/2019 - 15:52",
                "",
                false));
        mCourseClass.add( new CourseClass("",
                "",
                "",
                "",
                "",
                "2000DZD",
                "02:01:00",
                "",
                "27/02/2019 - 15:52",
                "",
                false));
        initializeAdapter();
        return parentView;
    }
    private void initializeAdapter(){
        courseHistoryAdapter adapter = new courseHistoryAdapter(mCourseClass);
        rv.setAdapter(adapter);
    }

}
