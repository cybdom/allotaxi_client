package com.cybdom.allotaxi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cybdom.allotaxi.fragments.course_history;
import com.cybdom.allotaxi.fragments.course_reservation;
import com.cybdom.allotaxi.fragments.home;
import com.cybdom.allotaxi.fragments.profile;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.maps.Style;

import java.util.List;

public class mainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PermissionsListener {

    private PermissionsManager permissionsManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("TRACE", "onCreate mainActivity");

        checkPermissions();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

//        Set User Info
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if ( user != null && !user.getDisplayName().isEmpty()) {

            View headerView = navigationView.getHeaderView(0);
            TextView sidebar_user_name = headerView.findViewById(R.id.sidebar_user_name);
            sidebar_user_name.setText(user.getDisplayName());
            home home_fragment = new home();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, home_fragment);
            transaction.commit();

        } else{
            Intent i = new Intent(this, login.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TRACE", "onDestroy mainActivity");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("TRACE", "onRestoreInstanceState mainActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TRACE", "onStop mainActivity");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TRACE", "onStart mainActivity");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if ( user == null ){
            Intent i = new Intent(this, login.class);
            startActivity(i);
            finish();
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.home){
            home home_fragment = new home();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, home_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else if ( menuItem.getItemId() == R.id.profile ) {
            profile profile_fragment = new profile();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, profile_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (menuItem.getItemId() == R.id.course){
            course_history course_history_fragment = new course_history();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, course_history_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (menuItem.getItemId() == R.id.reservation){
            course_reservation course_reservation_fragment = new course_reservation();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, course_reservation_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        return false;
    }

    // Checking permission

    private void checkPermissions() {
        Log.i("TRACE", "Check Permission");
        boolean permissionsGranted = PermissionsManager.areLocationPermissionsGranted(this);

        if (!permissionsGranted) {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onPermissionResult(boolean granted) {
        Log.i("TRACE", "onPermissionResult");
        if (granted) {
            Log.i("TRACE", "granted onPermissionResult");
            home.mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    home.enableLocationComponent(style, getApplicationContext());
                }
            });
        } else {
            Log.i("TRACE", "Else onPermissionResult");
            Toast.makeText(this, "Vous n'avez pas activer la localisation", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, "Veuillez activer la localisation", Toast.LENGTH_LONG).show();
        Log.i("TRACE", "onExplanationNeeded");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("TRACE", "onRequestPermissionsResult");
    }

}
