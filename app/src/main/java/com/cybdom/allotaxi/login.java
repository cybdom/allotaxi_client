package com.cybdom.allotaxi;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.cybdom.allotaxi.classes.userProfileClass;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class login extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    LinearLayout layout_error;
    CardView layout_signup;
    userProfileClass userSignupInfo;
    private int year, month, day;
    private Calendar calendar;
    FirebaseDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        database = FirebaseDatabase.getInstance();
        userSignupInfo = new userProfileClass();
        layout_error = findViewById(R.id.layout_error);
        layout_signup = findViewById(R.id.layout_signup);

        EditText user_date_naissance = findViewById(R.id.user_date_naissance);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        user_date_naissance.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if ( hasFocus ) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    showDate(dayOfMonth, monthOfYear, year);

                                }
                            }, year, month, day);
                    datePickerDialog.show();
                }
            }
        });


        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userNom = "";
        if ( user != null ){
            userNom = user.getDisplayName();
            if ( !userNom.isEmpty() ) {
                Intent i = new Intent(this, mainActivity.class);
                startActivity(i);
                finish();
            } else{
                layout_signup.setVisibility(View.VISIBLE);
            }
        }
        else {
            List<AuthUI.IdpConfig> providers = Collections.singletonList(
                    new AuthUI.IdpConfig.PhoneBuilder().build());
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(),
                    RC_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String userNom = "";
                if ( user != null  ){
                    userNom = user.getDisplayName();
                    if (userNom.isEmpty()) {
                        layout_signup.setVisibility(View.VISIBLE);
                    }
                } else{
                    Intent i = new Intent(this, mainActivity.class);
                    startActivity(i);
                }
            } else {
                layout_signup.setVisibility(View.GONE);
                layout_error.setVisibility(View.VISIBLE);
            }
        }
    }
    public void retry(View view){
        layout_error.setVisibility(View.GONE);
        List<AuthUI.IdpConfig> providers = Collections.singletonList(
                new AuthUI.IdpConfig.PhoneBuilder().build());
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }
    public void signUp(View view){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        EditText user_nom, user_prenom, user_date_naissance, user_email;
        Spinner user_gender;
        final ProgressDialog progressDialog = ProgressDialog.show(this, "", getResources().getString(R.string.wait_message));
        user_nom = findViewById(R.id.user_nom);
        user_prenom = findViewById(R.id.user_prenom);
        user_email = findViewById(R.id.user_email);
        user_date_naissance = findViewById(R.id.user_date_naissance);
        user_gender = findViewById(R.id.user_gender);

        userSignupInfo.setUser_nom(user_nom.getText().toString());
        userSignupInfo.setUser_prenom(user_prenom.getText().toString());
        userSignupInfo.setUser_email(user_email.getText().toString());
        userSignupInfo.setUser_date_naissance(user_date_naissance.getText().toString());
        userSignupInfo.setUser_gender(user_gender.getSelectedItem().toString());
        userSignupInfo.setUser_note(5.0);
        if ( user != null ) {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(user_nom.getText().toString() + " " + user_prenom.getText().toString())
                .build();
            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressDialog.dismiss();
                            if (task.isSuccessful()) {
                                database.getReference().child("client").child(user.getUid()).child("profile").setValue(userSignupInfo)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Intent i = new Intent(getApplicationContext(), mainActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(login.this, R.string.try_again_error_message, Toast.LENGTH_SHORT).show();
                                                layout_signup.setVisibility(View.GONE);
                                                layout_error.setVisibility(View.VISIBLE);
                                            }
                                        });
                            }
                        }
                    });
        }
    }

    private void showDate(int year, int month, int day) {
        EditText user_date_naissance = findViewById(R.id.user_date_naissance);
        month += 1;
        user_date_naissance.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }
}
