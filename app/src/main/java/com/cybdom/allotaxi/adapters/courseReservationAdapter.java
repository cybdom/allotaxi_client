package com.cybdom.allotaxi.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.allotaxi.R;
import com.cybdom.allotaxi.classes.CourseClass;

import java.util.List;

public class courseReservationAdapter extends RecyclerView.Adapter<courseReservationAdapter.courseReservationViewHolder>{
    public static class courseReservationViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView course_date,course_origin,course_destination, course_status;
        courseReservationViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            course_date = itemView.findViewById(R.id.course_date);
            course_origin = itemView.findViewById(R.id.course_origin);
            course_destination = itemView.findViewById(R.id.course_destination);
            course_status = itemView.findViewById(R.id.course_status);
        }
    }
    List<CourseClass> mCourseClasses;
    public courseReservationAdapter(List<CourseClass> mCourseClass){
        this.mCourseClasses = mCourseClass;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public courseReservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_course_reservation,parent,false);
        courseReservationViewHolder mvh = new courseReservationViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull courseReservationViewHolder mViewHolder, final int i ) {
        mViewHolder.course_date.setText(mCourseClasses.get(i).getCourse_date());
//        mViewHolder.course_origin.setText(mCourseClasses.get(i).getCourseOriginText);
//        mViewHolder.course_destination.setText(mCourseClasses.get(i).getCourseDestinationText());
        mViewHolder.course_status.setText("En attente");
    }
    @Override
    public int getItemCount(){
        return mCourseClasses.size();
    }
}
